import { ActionTypes } from "@mui/base";

export const initialState = {
  user: null,
  playlists: [],
  playing: false,
  item: null,
  spotify: null,
  discover_weekly: null,
  top_artists: null,
  playing: false,
  // REMOVE after finished developing..
  // token:
  // 'BQCe6WXnz2k1EBLzBLULDr72QiBpW--nomGV_OfkFbPClxMP0xmL5XE9GItwcc6oUCyoLBhJVR3Ce9M5lIGDDC-seMJAYrTAJKkSkF9V-jcNgGb6DD7fjLvxgwkyZ4rnzl6AT_LTYQNSqx0U9xj_Zwb8aK9gilT3sV6spEiN1A3nku-Pa5i0lc5r8KF5ToTJMpfP0uQ',
};

const reducer = (state, action) => {
console.log(action);

// Action -> type, [payload]

  switch(action.type) {
    case 'SET_USER':
      return {
        ...state, // very important this keeps state
        user: action.user,
      };

      case 'SET_TOKEN':
        return {
          ...state,
          token: action.token,
        };

      case 'SET_PLAYLISTS':
        return {
          ...state,
          playlists: action.playlists,
        };

      case 'SET_DISCOVER_WEEKLY':
        return {
          ...state,
          discover_weekly: action.discover_weekly,
        }

      case 'SET_PLAYING':
        return {
          ...state,
          playing: action.playing,
        };

      case "SET_SPOTIFY":
        return {
          ...state,
          spotify: action.spotify,
        };

      case "SET_ITEM":
        return {
          ...state,
          item: action.item,
        };

      default:
        return state;
  };
}

export default reducer;
